#!/bin/sh

pushd ${WORKING_ROOT}

	mvn install:install-file -DgroupId=org.oscarehr.hnr -DartifactId=hnr_client -Dversion=SNAPSHOT -Dpackaging=jar -Dfile=target/hnr_client.jar -DcreateChecksum=true -DgeneratePom=true -DlocalRepositoryPath=../caisi_integrator2/lib -DlocalRepositoryId=local-cvs
	rm -f ~/.m2/repository/org/oscarehr/hnr/hnr_client/SNAPSHOT/hnr_client-SNAPSHOT.jar

popd
