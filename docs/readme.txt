This is the Health Number Registry project.

The purpose of this project is to provide an online registry which multiple / different 
applications can interface with to use as a central validated health number database.

This project should be a standard maven project.

Running the maven tests will create the database tables for you.
Before the tests work it expects a blank schema is made with the name as configured in
the spring_dao.xml, i.e. "hnr", with associated user/password. The test scripts
will drop all tables and create new tables with test data upon initialisation, the created
tables can be the basis for a running system. The database is currently assumed to be Mysql
and using INNODB as the default table, you will need to configure this in the my.cnf file.

When you start tomcat you may pass it a system property hnr_properties=<property_file>
which overrides any values found in the config.properties file. You can also pass in
log4j.override.configuration=<path to log4j.xml> which will configure those settings
on top of the ones in the distributed log4j.xml file. 

Out of the box, the default configuration has https_endpoint_url_base=https://127.0.0.1:8096/hnr/ws
this is most likely incorrect for everyone, you must change the endpoint to be what ever the clients will
use to resolve to that location, i.e. the ip address as seen from the client, and the contextPath you've
deployed to. This is a CXF oddity in that we need to explicitly configure it.

If you're using an IDE to work with this code base, you're going to want to "ignore missing serialVersionUID".
We do not support compatibility between compiled class files except with in the exact same build instance.
Beyond the above change, there should generally be no errors or warnings of any types in the source code. 

There are utility methods in the utils directory, they're not required, they just help 
with a few commonly run tasks. The build_client_stubs.xml has 2 targets of interest.
By default it runs build_client_stubs which builds the client stubs and produces
a jar in the target directory. There's also a run_test_client target which will build
and run a few simple tests against the server.
