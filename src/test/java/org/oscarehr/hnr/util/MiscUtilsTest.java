package org.oscarehr.hnr.util;

import static org.junit.Assert.assertEquals;

import org.apache.log4j.Logger;
import org.junit.Test;

public class MiscUtilsTest
{
	@Test
	public void getLoggerTest()
	{
		Logger logger = MiscUtils.getLogger();
		assertEquals(MiscUtilsTest.class.getName(), logger.getName());
	}

	@Test
	public void getAsBracketedListTest()
	{
		String[] list =
		{
		        "a", "b", "c"
		};

		String result = MiscUtils.getAsBracketedList(list, false);
		assertEquals("(a,b,c)", result);

		result = MiscUtils.getAsBracketedList(list, true);
		assertEquals("('a','b','c')", result);
	}
}
