package org.oscarehr.hnr.dao;

import java.io.IOException;
import java.util.Date;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.oscarehr.hnr.util.LoggedInInfo;
import org.oscarehr.hnr.util.SpringUtils;

public class DaoTestFixtures
{
	private static UserDao userDao = (UserDao)SpringUtils.getBean("userDao");
	private static ClientDao clientDao = (ClientDao)SpringUtils.getBean("clientDao");

	protected static User defaultUser = null;

	@BeforeClass
	public static void classSetUp() throws IOException
	{
		InitialiseDataStore.recreateDatabase();
		createDefaultUser();

		LoggedInInfo.setLoggedInInfoToCurrentClassAndMethod();

		createDefaultClient();
	}

	@AfterClass
	public static void classTearDown()
	{
		LoggedInInfo.loggedInInfo.remove();
	}

	private static void createDefaultUser()
	{
		User user = new User();
		user.setName("test_user");
		user.setPassword("test_password");
		user.setContactInfo("test contact info");

		userDao.persist(user);

		defaultUser = user;
	}

	private static void createDefaultClient() throws IOException
	{
		Client client = new Client();
		client.setBirthDate(new Date());
		client.setCity("test city");
		client.setUpdatedBy("ted at home testing");
		client.setFirstName("test first name ");
		client.setGender(Client.Gender.M);
		client.setHin("test HIN number");
		client.setHinType(" test hin type");
		client.setHinVersion("test hin version ");
		client.setLastName(" test last name");
		client.setProvince("Bc ");
		client.setSin(" test my SIN");
		client.setHidden(false);
		client.setHiddenChangeDate(new Date());

		ClientTest.setTestImage(client);

		clientDao.persist(client);
	}
}