package org.oscarehr.hnr.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.oscarehr.hnr.util.SpringUtils;

public class ClientTest extends DaoTestFixtures
{
	private static ClientDao clientDao=(ClientDao)SpringUtils.getBean("clientDao");
	private static EventLogDao eventLogDao=(EventLogDao)SpringUtils.getBean("eventLogDao");	

	public static Client createPersistedClient(long seed) throws IOException
	{
		Client client=new Client();
		client.setBirthDate(new Date());
		client.setCity("my city");
		client.setUpdatedBy("ted at home testing");
		client.setUpdated(new Date());
		client.setFirstName("my first name "+seed);
		client.setGender(Client.Gender.M);
		client.setHin(""+seed);
		client.setHinType(" my hIn type");
		client.setHinVersion("my hIn version ");
		client.setHidden(false);
		client.setHiddenChangeDate(new Date());
		
		GregorianCalendar cal=new GregorianCalendar(2006, 4, 7);
		client.setHinValidStart(cal.getTime());
		
		GregorianCalendar cal2=new GregorianCalendar(2009, 3, 14);
		client.setHinValidEnd(cal2.getTime());
		
		client.setLastName(" my last name "+seed);
		client.setProvince("Bc ");
		client.setSin(" my SIN "+seed);
		client.setStreetAddress(" my street addr ");
		
		setTestImage(client);

		clientDao.persist(client);
		
		return(client);
	}

	public static void setTestImage(Client client) throws IOException
	{
		InputStream is=ClientTest.class.getResourceAsStream("/test_image.jpg");
		byte[] sourceImage=IOUtils.toByteArray(is);
		client.setImage(sourceImage);
	}
	
	@Test
	public void clientTest() throws IOException
	{
		long beforeLogCount=eventLogDao.getCountAll();
		long beforeArchiveCount=clientDao.countArchivedRecords();

		//--- create ---
		long seed=System.currentTimeMillis();
		Client client=createPersistedClient(seed);
		
		long afterLogCount=eventLogDao.getCountAll();
		assertEquals(beforeLogCount+1, afterLogCount);

		long afterArchiveCount=clientDao.countArchivedRecords();
		assertEquals(beforeArchiveCount, afterArchiveCount);

		//--- find ---
		Client client1=clientDao.findByLinkingIdUnlockedUnhidden(client.getId());
		
		assertNotNull(client.getBirthDate());
		assertEquals("my city", client.getCity());
		assertNotNull(client.getUpdated());
		assertEquals("ted at home testing", client.getUpdatedBy());
		assertEquals("my first name "+seed, client.getFirstName());
		assertEquals(Client.Gender.M, client.getGender());
		assertEquals(""+seed, client.getHin());
		assertEquals("my hin type", client.getHinType());
		assertEquals("MY HIN VERSION", client.getHinVersion());
		
		GregorianCalendar cal=new GregorianCalendar();
		cal.setTime(client.getHinValidStart());
		assertEquals(2006, cal.get(Calendar.YEAR));
		assertEquals(4, cal.get(Calendar.MONTH));
		assertEquals(7, cal.get(Calendar.DAY_OF_MONTH));
		
		cal.setTime(client.getHinValidEnd());
		assertEquals(2009, cal.get(Calendar.YEAR));
		assertEquals(3, cal.get(Calendar.MONTH));
		assertEquals(14, cal.get(Calendar.DAY_OF_MONTH));
		
		assertEquals("my last name "+seed, client.getLastName());
		assertEquals("bc", client.getProvince());
		assertEquals("my sin "+seed, client.getSin());
		assertEquals("my street addr", client.getStreetAddress());
		assertTrue(client.getImage().length>20);
						
		// check log
		afterLogCount=eventLogDao.getCountAll();
		assertEquals(beforeLogCount+2, afterLogCount);
		
		afterArchiveCount=clientDao.countArchivedRecords();
		assertEquals(beforeArchiveCount, afterArchiveCount);

		//--- update ---
		client1.setLastName("the updated lastname");
        clientDao.merge(client1);

		afterArchiveCount=clientDao.countArchivedRecords();
		assertEquals(beforeArchiveCount+1, afterArchiveCount);
        
		client1.setFirstName("the updated firstname");
        clientDao.merge(client1);

		afterArchiveCount=clientDao.countArchivedRecords();
		assertEquals(beforeArchiveCount+2, afterArchiveCount);
        
		//--- delete ---
		try
        {
	        clientDao.remove(client1.getId());
	        fail("exception should have been thrown, remove is not allowed");
        }
        catch (Exception e)
        {
	        // okay this is good
        }        
	}
	
	@Test
	public void fuzzyMatchTest() throws IOException
	{
		Client client=new Client();
		client.setFirstName("first");
		client.setLastName("last");
		client.setUpdatedBy("ted at home slightly injured by snowboarding accident");
		client.setBirthDate(new Date());
		client.setHin("5555555555");
		client.setHinType("ON");
		client.setHidden(false);
		client.setHiddenChangeDate(new Date());
		setTestImage(client);
		clientDao.persist(client);
		
		List<MatchingClientScore> results=clientDao.findMatchingClients(10, 1, "first", "last", null, null, null, null, null, null);
		assertEquals(3,results.size());
		
		results=clientDao.findMatchingClients(10, 1, "first", "xxx", null, null, null, null, null, null);
		assertEquals(3,results.size());
		
		results=clientDao.findMatchingClients(10, 1, "xxx", "last", null, null, null, null, null, null);
		assertEquals(3,results.size());

		results=clientDao.findMatchingClients(10, 1, "xxx", "name", null, null, null, null, null, null);
		assertEquals(2,results.size());

		results=clientDao.findMatchingClients(10, 1, "test", "xxx", null, null, null, null, null, null);
		assertEquals(1,results.size());

		results=clientDao.findMatchingClients(10, 1, "xxx", "xxx", null, null, null, null, null, null);
		assertEquals(0,results.size());
	}
}
