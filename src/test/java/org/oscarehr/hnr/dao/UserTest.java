package org.oscarehr.hnr.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;
import org.oscarehr.hnr.util.SpringUtils;

public class UserTest extends DaoTestFixtures
{
	private static UserDao userDao=(UserDao)SpringUtils.getBean("userDao");
	
	public static User createPersistedUser()
	{
		User user=new User();
		user.setName("my_name");
		user.setPassword("my password");
		user.setContactInfo("my contact info");
		
		userDao.persist(user);
		
		return(user);
	}
	
	@Test
	public void userTest()
	{
		//--- create ---
		User user=createPersistedUser();
		
		//--- find ---
		User user1=userDao.find(user.getId());
		assertEquals("my_name", user.getName());
		assertEquals("my password", user.getPassword());
		assertEquals("my contact info", user.getContactInfo());
						
		User user2=userDao.findByName("my_name");
		assertEquals(user.getId(), user2.getId());
		
		//--- update ---
		user1.setContactInfo("update contact info");
        userDao.merge(user1);
		
		User user3=userDao.find(user.getId());
		assertEquals("update contact info", user3.getContactInfo());
		
		//--- delete ---
		try
        {
	        userDao.remove(user1.getId());
	        fail("exception should have been thrown, remove is not allowed");
        }
        catch (Exception e)
        {
	        // okay this is good
        }
	}
	
}
