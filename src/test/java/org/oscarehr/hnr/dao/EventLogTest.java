package org.oscarehr.hnr.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.IOException;

import org.junit.Test;
import org.oscarehr.hnr.dao.EventLog.EventType;
import org.oscarehr.hnr.util.SpringUtils;

public class EventLogTest extends DaoTestFixtures
{
	private static EventLogDao eventLogDao=(EventLogDao)SpringUtils.getBean("eventLogDao");
	
	@Test
	public void eventLogTest() throws IOException
	{
		long seed=System.currentTimeMillis();
		Client client=ClientTest.createPersistedClient(seed);
		
		//--- create ---
		EventLog eventLog=eventLogDao.createEvent(EventType.READ, client);
		
		//--- find ---
		EventLog eventLog1=eventLogDao.find(eventLog.getId());
		
		assertNotNull(eventLog1.getDate());
		assertEquals(DaoTestFixtures.class.getName()+".classSetUp",eventLog1.getLocalSourceCode());
		assertEquals(EventLog.EventType.READ,eventLog1.getEventType());
		assertEquals(Client.class.getName(),eventLog1.getDataType());
		assertEquals(client.getId().toString(),eventLog1.getDataId());
						
		//--- update ---
		eventLog1.setAccessorTrail("updated creator by");

		try
        {
			eventLogDao.merge(eventLog1);
	        fail("exception should have been thrown, update is not allowed");
        }
        catch (Exception e)
        {
	        // okay this is good
        }
		
		//--- delete ---
		try
        {
			eventLogDao.remove(eventLog1.getId());
	        fail("exception should have been thrown, remove is not allowed");
        }
        catch (Exception e)
        {
	        // okay this is good
        }
	}
	
}
