package org.oscarehr.hnr.util;

import org.apache.commons.lang.StringUtils;

public class HinValidator
{
	/**
	 * This method will return false if the hin is clearly invalid. It will return
	 * true otherwise. This means that the default is true and anything it can't
	 * figure out will return true. As an example if the hinType is null
	 * then there's no validation algorithm so it will return true. 
	 */
	public static boolean isValid(String hin, String hinType)
	{
		if ("on".equals(hinType)) return(isValidOntario(hin));
		else return(true);
	}
	
	private static boolean isValidOntario(String hin)
	{
		if (hin==null) return(false);
		if (hin.length()!=10) return(false);
		if (!StringUtils.isNumeric(hin)) return(false);
		
		return(true);
	}
}
