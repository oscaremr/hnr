package org.oscarehr.hnr.util;

import org.apache.openjpa.jdbc.ant.MappingToolTask;
import org.apache.openjpa.jdbc.conf.JDBCConfigurationImpl;
import org.apache.openjpa.lib.conf.ConfigurationImpl;

public class SpringJpaMappingToolTask extends MappingToolTask
{
	@Override
	public ConfigurationImpl newConfiguration()
	{
		System.setProperty("catalina.base", System.getProperty("java.io.tmpdir"));
		
		JDBCConfigurationImpl conf=(JDBCConfigurationImpl)super.newConfiguration();
		conf.setConnectionDriverName(ConfigUtils.getProperty("db_driver"));
		conf.setConnectionURL(ConfigUtils.getProperty("db_url"));
		conf.setConnectionUserName(ConfigUtils.getProperty("db_user"));
		conf.setConnectionPassword(ConfigUtils.getProperty("db_password"));
		
		return(conf);
	}
}
