package org.oscarehr.hnr.util;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

public class SpringPropertyConfigurer extends PropertyPlaceholderConfigurer
{
	public SpringPropertyConfigurer()
	{
		setProperties(ConfigUtils.getProperties());
	}
}