package org.oscarehr.hnr.util;

public class InvalidHinException extends Exception
{
	public InvalidHinException(String hin, String hinType)
	{
		super("The hin entered does not pass validation. hin="+hin+", hinType="+hinType);
	}	
	
	public InvalidHinException(String hin, String hinType, Throwable t)
	{
		super("The hin entered does not pass validation. hin="+hin+", hinType="+hinType, t);
	}
}
