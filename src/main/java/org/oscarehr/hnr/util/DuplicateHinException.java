package org.oscarehr.hnr.util;

public class DuplicateHinException extends Exception
{
	public DuplicateHinException(String hin)
	{
		super("The hin entered is a duplicate of one already in the system. hin="+hin);
	}	
	
	public DuplicateHinException(String hin, Throwable t)
	{
		super("The hin entered is a duplicate of one already in the system. hin="+hin, t);
	}
}
