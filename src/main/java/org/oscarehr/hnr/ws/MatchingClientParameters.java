/**
 * 
 */
package org.oscarehr.hnr.ws;

import java.util.Date;

public class MatchingClientParameters
{
	public int maxEntriesToReturn;
	public int minScore;
	public String firstName;
	public String lastName;
	public Date birthDate;
	public String hin;
}