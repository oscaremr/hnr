package org.oscarehr.hnr.ws;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import org.oscarehr.hnr.dao.User;
import org.oscarehr.hnr.util.LoggedInInfo;

public class AbstractWs
{
	@Resource
    protected WebServiceContext context;
	
	protected HttpServletRequest getHttpServletRequest()
	{
		MessageContext messageContext=context.getMessageContext();
		HttpServletRequest request=(HttpServletRequest)messageContext.get(MessageContext.SERVLET_REQUEST);
		return(request);
	}
	
	protected User getLoggedInUser()
	{
		return(LoggedInInfo.loggedInInfo.get().loggedInUser);
	}
}
