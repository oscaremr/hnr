package org.oscarehr.hnr.ws;

import java.util.ArrayList;
import java.util.Date;

import javax.jws.WebService;
import javax.persistence.EntityExistsException;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.oscarehr.hnr.dao.Client;
import org.oscarehr.hnr.dao.ClientDao;
import org.oscarehr.hnr.dao.MatchingClientScore;
import org.oscarehr.hnr.util.DuplicateHinException;
import org.oscarehr.hnr.util.HinValidator;
import org.oscarehr.hnr.util.InvalidHinException;
import org.oscarehr.hnr.util.MiscUtils;
import org.oscarehr.hnr.util.SpringUtils;
import org.springframework.dao.DataIntegrityViolationException;

@WebService
public class ClientWs extends AbstractWs
{
	private Logger logger = MiscUtils.getLogger();

	// not autowireable because jax:jw is not spring instantiated
	private ClientDao clientDao = (ClientDao)SpringUtils.getBean("clientDao");

	public ArrayList<MatchingClientScore> getMatchingClients(MatchingClientParameters parameters)
	{
		try
		{
			ArrayList<MatchingClientScore> results = clientDao.findMatchingClients(parameters.maxEntriesToReturn, parameters.minScore, parameters.firstName, parameters.lastName, parameters.birthDate, parameters.hin, null, null, null, null);
			return(results);
		}
		catch (Exception e)
		{
			logger.error("unexpected error", e);
			return(null);
		}
	}

	public Client getClientData(Integer linkingId)
	{
		try
		{
			Client client = clientDao.findByLinkingIdUnlockedUnhidden(linkingId);
			return(client);
		}
		catch (Exception e)
		{
			logger.error("unexpected error", e);
		}

		return(null);
	}

	/**
	 * @return the linkingId for the client, it is either the one passed in or a new one if null was passed in.
	 * @throws DuplicateHinException if the HIN is a duplicate of one already in the system.
	 * @throws InvalidHinException 
	 */
	public Integer setClientData(Client client) throws DuplicateHinException, 
		InvalidHinException
	{
		try
		{
			// check hin validation
			if (!HinValidator.isValid(client.getHin(), client.getHinType())) throw(new InvalidHinException(client.getHin(), client.getHinType()));
			
			// update the update date.
			client.setUpdated(new Date());

			// make sure we have the lastest consent status, if not copy the latest consent to the records.
			if (client.getId() != null)
			{
				Client oldClient = clientDao.find(client.getId());
				if (oldClient != null) // it's only really possible to be null if a client has invalid data, but since we can't guarantee what's going on in client databases... we'll check ourselves
				{
					// copy consent info to new client,
					// then merge new client

					if (client.getHiddenChangeDate() == null)
					{
						client.setHidden(oldClient.isHidden());
						client.setHiddenChangeDate(oldClient.getHiddenChangeDate());
					}
					else if (oldClient.getHiddenChangeDate() == null)
					{
						// do nothing, take what ever is passed in.
					}
					else
					// this means neither is null
					{
						if (oldClient.getHiddenChangeDate().after(client.getHiddenChangeDate()))
						{
							client.setHidden(oldClient.isHidden());
							client.setHiddenChangeDate(oldClient.getHiddenChangeDate());
						}
					}
					
					clientDao.merge(client);
				}
				else
				{
					// save the new client, client id was given but they don't actually exist
					client.setLinkingId(null);
					clientDao.persist(client);
				}
			}
			else
			{
				// save the new client
				clientDao.persist(client);
			}
			
			return(client.getLinkingId());
		}
		catch (InvalidHinException e)
		{
			throw(e);
		}
		catch (EntityExistsException e)
		{
			logger.debug("Entity Exists exception", e);
			throw(new DuplicateHinException(client.getHin(),e));
		}
		catch (DataIntegrityViolationException e)
		{
			if (e.getCause() instanceof EntityExistsException)
			{
				logger.debug("Entity Exists exception", e);
				throw(new DuplicateHinException(client.getHin(),e));
			}
			else
			{
				logger.error("unexpected error", e);
			}
		}
		catch (Exception e)
		{
			logger.error("unexpected error", e);
		}

		return(null);
	}

	public void setClientHidden(Integer clientLinkingId, boolean hidden, Date hiddenChangeDate)
	{
		try
		{
			Client oldClient = clientDao.find(clientLinkingId);

			// make sure the consent is newer than the ones we already have first.
			if (oldClient != null && (oldClient.getHiddenChangeDate() == null || hiddenChangeDate.after(oldClient.getHiddenChangeDate())))
			{
				Client client = new Client();
				BeanUtils.copyProperties(client, oldClient);

				client.setUpdated(new Date());
				client.setHidden(hidden);
				client.setHiddenChangeDate(hiddenChangeDate);

				clientDao.persist(client);
			}
		}
		catch (Exception e)
		{
			logger.error("unexpected error", e);
		}
	}
}
