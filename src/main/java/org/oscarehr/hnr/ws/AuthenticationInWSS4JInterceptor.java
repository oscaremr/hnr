package org.oscarehr.hnr.ws;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.xml.namespace.QName;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.headers.Header;
import org.apache.cxf.ws.security.wss4j.WSS4JInInterceptor;
import org.apache.log4j.Logger;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.WSPasswordCallback;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.oscarehr.hnr.dao.User;
import org.oscarehr.hnr.dao.UserDao;
import org.oscarehr.hnr.util.LoggedInInfo;
import org.oscarehr.hnr.util.MiscUtils;
import org.oscarehr.hnr.util.SpringUtils;
import org.w3c.dom.Element;

public class AuthenticationInWSS4JInterceptor extends WSS4JInInterceptor implements CallbackHandler
{
	private static final Logger logger = MiscUtils.getLogger();
	private static final QName AUDIT_TRAIL_QNAME = new QName("http://oscarehr.org/caisi", "auditTrail", "caisi");

	// the interceptor is not in a context so we can't use autowire
	private UserDao userDao = (UserDao)SpringUtils.getBean("userDao");

	private ArrayList<String> excludes = null;

	public AuthenticationInWSS4JInterceptor()
	{
		HashMap<String, Object> properties = new HashMap<String, Object>();
		properties.put(WSHandlerConstants.ACTION, WSHandlerConstants.USERNAME_TOKEN);
		properties.put(WSHandlerConstants.PASSWORD_TYPE, WSConstants.PW_TEXT);
		properties.put(WSHandlerConstants.PW_CALLBACK_REF, this);

		setProperties(properties);
	}

	@Override
	public void handleMessage(SoapMessage message)
	{
		// if excluded from authentication
		String basePath = (String)message.get(SoapMessage.BASE_PATH);
		if (isExcluded(basePath)) return;
		
		String auditTrail = getAuditTrail(message);
		if (logger.isDebugEnabled()) logger.debug("Audit Trail = " + auditTrail);
		LoggedInInfo loggedInInfo = LoggedInInfo.loggedInInfo.get();
		loggedInInfo.loggedInUserAuditTrail = auditTrail;

		super.handleMessage(message);
	}

	public void setExcludes(ArrayList<String> excludes)
	{
		this.excludes = excludes;
	}

	private boolean isExcluded(String s)
	{
		// this means it's a response to my request - still registers as in bound even though it's just an inbound response.  
		if (s == null) return(true);

		for (String x : excludes)
		{
			if (s.endsWith(x)) return(true);
		}

		return(false);
	}

	@Override
	public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException
	{
		for (Callback callback : callbacks)
		{
			if (callback instanceof WSPasswordCallback)
			{
				WSPasswordCallback wsPasswordCallback = (WSPasswordCallback)callback;

				String username = wsPasswordCallback.getIdentifier();
				String password = wsPasswordCallback.getPassword();

				if (logger.isDebugEnabled())
				{
					logger.debug("WSPasswordCallback: u=" + username + ", p=" + password + ", p type=" + wsPasswordCallback.getPasswordType());
				}

				User user = getByUserPassword(username, password);
				if (user == null)
				{
					String errorString = "Invalid username/password attempted. username=" + username;
					logger.warn(errorString);

					// no need to clear audit trail as it should clear by the filter's request exist.

					throw(new SecurityException(errorString));
				}
				else
				{
					LoggedInInfo.loggedInInfo.get().loggedInUser = user;
					return;
				}
			}
		}

		throw(new SecurityException("Odd, no password check call back made."));
	}

	/**
	 * @return User if valid or null if invalid
	 */
	private User getByUserPassword(String username, String password)
	{
		if (username == null || password == null) return(null);

		User user = userDao.findByName(username);
		if (user != null && password.equals(user.getPassword())) return(user);
		else return(null);
	}

	private String getAuditTrail(SoapMessage message)
	{
		Header header = message.getHeader(AUDIT_TRAIL_QNAME);
		if (header != null)
		{
			Element element = (Element)header.getObject();
			if (element != null) return(element.getTextContent());
		}

		return(null);
	}
}
