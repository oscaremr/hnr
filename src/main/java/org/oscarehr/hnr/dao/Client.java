package org.oscarehr.hnr.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;
import org.oscarehr.hnr.dao.EventLog.EventType;
import org.oscarehr.hnr.util.ImageIoUtils;
import org.oscarehr.hnr.util.MiscUtils;
import org.oscarehr.hnr.util.SpringUtils;

/**
 * For business reasons, before any row is modified, 
 * the contents of the row needs to be copied to the ClientArchive table.
 */
@Entity
public class Client extends AbstractModel<Integer> implements Comparable<Client>
{
	private EventLogDao eventLogDao=(EventLogDao)SpringUtils.getBean("eventLogDao");
	
	/**
	 * Male, female, transgendered, other, undefined, null is unknown/unspecified.
	 */
	public enum Gender
	{
		M,F,T,O,U
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id=null;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	private Date updated = new Date();
	
	/** hidden signifies if the client wants people to see their record or not */
	@Column(nullable=false, columnDefinition="tinyint(1)")
	private boolean hidden=true;
	
	/** the hidden Consent date signifies when the last consent to hide or show this record was set */ 
	@Temporal(TemporalType.TIMESTAMP)
	private Date hiddenChangeDate = null;
	
	/** lockbox is the privacy regulation lockbox concept */
	@Column(nullable=false, columnDefinition="tinyint(1)")
	private boolean lockbox=false;
	
	@Column(nullable=false, length=255)
	private String updatedBy=null;

	@Column(length=255)
	private String firstName = null;
	
	@Column(length=255)
	private String lastName = null;
	
	@Temporal(TemporalType.DATE)
	private Date birthDate = null;
	
	@Enumerated(EnumType.STRING)
	@Column(length=1)
	private Gender gender = null;
	
	@Column(length=64, unique=true)
	private String hin = null;
	
	@Column(length=64)
	private String hinType = null;
	
	@Column(length=64)
	private String hinVersion = null;
	
	@Temporal(TemporalType.DATE)
	private Date hinValidStart = null;
	
	@Temporal(TemporalType.DATE)
	private Date hinValidEnd = null;
	
	@Column(length=64)
	private String sin = null;
	
	@Column(length=3)
	private String province = null;
	
	@Column(length=64)
	private String city = null;
	
	@Column(length=128)
	private String streetAddress = null;

	@Column(columnDefinition="mediumblob")
	private byte[] image = null;

	@Override
    public Integer getId()
    {
	    return id;
    }

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = StringUtils.trimToNull(firstName);
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = StringUtils.trimToNull(lastName);
	}

	public Date getBirthDate()
	{
		return birthDate;
	}

	public void setBirthDate(Date birthDate)
	{
		this.birthDate = birthDate;
	}

	public Gender getGender()
	{
		return gender;
	}

	public void setGender(Gender gender)
	{
		this.gender = gender;
	}

	public String getHin()
	{
		return hin;
	}

	public void setHin(String hin)
	{
		this.hin = MiscUtils.trimToNullLowerCase(hin);
	}

	public String getHinType()
    {
    	return hinType;
    }

	public void setHinType(String hinType)
    {
    	this.hinType = MiscUtils.trimToNullLowerCase(hinType);
    }

	public String getHinVersion()
	{
		return hinVersion;
	}

	public void setHinVersion(String hinVersion)
	{
		this.hinVersion = MiscUtils.trimToNullUpperCase(hinVersion);
	}

	public Date getHinValidStart()
	{
		return hinValidStart;
	}

	public void setHinValidStart(Date hinValidStart)
	{
		this.hinValidStart=hinValidStart;
	}

	public Date getHinValidEnd()
	{
		return hinValidEnd;
	}

	public void setHinValidEnd(Date hinValidEnd)
	{
		this.hinValidEnd=hinValidEnd;
	}

	public String getSin()
	{
		return sin;
	}

	public void setSin(String sin)
	{
		this.sin = MiscUtils.trimToNullLowerCase(sin);
	}

	public String getProvince()
	{
		return province;
	}

	public void setProvince(String province)
	{
		this.province = MiscUtils.trimToNullLowerCase(province);
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(String city)
	{
		this.city = MiscUtils.trimToNullLowerCase(city);
	}

	public String getStreetAddress()
	{
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress)
	{
		this.streetAddress = MiscUtils.trimToNullLowerCase(streetAddress);
	}

	public boolean isHidden()
    {
    	return hidden;
    }

	public void setHidden(boolean hidden)
	{
		this.hidden=hidden;
	}
	
	public Date getHiddenChangeDate()
	{
		return hiddenChangeDate;
	}

	public void setHiddenChangeDate(Date hiddenChangeDate)
	{
		this.hiddenChangeDate=hiddenChangeDate;
	}

	public boolean isLockbox()
    {
    	return lockbox;
    }

	public void setLockbox(boolean lockbox)
	{
		this.lockbox=lockbox;
	}
	
	public Date getUpdated()
	{
		return updated;
	}

	public void setUpdated(Date updated)
	{
		this.updated = updated;
	}

	public String getUpdatedBy()
	{
		if (updatedBy==null) throw(new IllegalArgumentException("updatedBy can't be null"));
		return updatedBy;
	}

	/**
	 * Update by is any string that's meant to be human readable
	 * denoting the source of this record.
	 * As an example it might say 'doctor bob at seaton house ontario', or
	 * it might just say 'ontario ministry registry' or something human interpretable,
	 * not machine parsable. 
	 */
	public void setUpdatedBy(String updatedBy)
	{
		this.updatedBy = updatedBy;
	}

	public byte[] getImage()
	{
		return image;
	}

	public void setImage(byte[] original)
	{
		this.image = ImageIoUtils.scaleJpgSmallerProportionally(original, 200, 200, .90f);
	}
	
	/**
	 * These methods are needed to expose the id as it's hidden by default
	 */
	public void setLinkingId(Integer id)
	{
		this.id=id;
	}
	
	/**
	 * These methods are needed to expose the id as it's hidden by default
	 */
	public Integer getLinkingId()
	{
		return(id);
	}
	
	@Override
	public int compareTo(Client o)
	{
		return(getId() - o.getId());
	}

	@PreRemove
	protected void jpaPreventDelete()
	{
		throw(new UnsupportedOperationException("Remove is not allowed for this type of item."));
	}

	@PreUpdate
	protected void makeArchivedRow()
	{
		ClientDao.archivePreviousRecords(id);
	}
	
	@PostLoad
	protected void logRead()
	{
		eventLogDao.createEvent(EventType.READ, this);
	}
	
	@PostPersist
	@PostUpdate
	protected void logWrite()
	{
		eventLogDao.createEvent(EventType.WRITE, this);
	}
}
