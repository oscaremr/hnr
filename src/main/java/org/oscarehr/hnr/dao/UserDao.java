package org.oscarehr.hnr.dao;

import javax.persistence.Query;

import org.oscarehr.hnr.util.MiscUtils;
import org.springframework.stereotype.Repository;

@Repository
public class UserDao extends AbstractDao<User>
{
    public UserDao()
	{
		modelClass=User.class;
	}

	public User findByName(String name)
	{
		name = MiscUtils.validateAndNormaliseUserName(name);

		Query query = entityManager.createQuery("select x from "+modelClass.getSimpleName()+" x where x.name=?1");
		query.setParameter(1, name);

		return(getSingleResultOrNull(query));
	}
}
