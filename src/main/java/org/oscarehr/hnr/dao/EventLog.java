package org.oscarehr.hnr.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;
import org.apache.openjpa.persistence.DataCache;

/**
 * The usage of this class / dao is for creation only since this is a log. Once this row is created it should never be deleted or updated.
 */
@Entity
@DataCache(enabled = false)
public class EventLog extends AbstractModel<Integer>
{
	public enum EventType
	{
		READ, SEARCH_LIST, WRITE, DELETE
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id = null;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Date date = new Date();

	@Column(columnDefinition = "text")
	private String accessorTrail = null;

	private Integer localUserId = null;

	@Column(columnDefinition = "text")
	private String localSourceCode=null;
	
	@Column(nullable = false, length = 32)
	@Enumerated(EnumType.STRING)
	private EventType eventType = null;

	@Column(nullable = false, length = 255)
	private String dataType = null;

	@Column(length = 255)
	private String dataId = null;

	@Override
	public Integer getId()
	{
		return id;
	}

	/**
	 * The creatorTrail is a human readable string denoting who 
	 * created this event. This should be a growing string based 
	 * on how far along this information was requested from or passed
	 * along to, i.e. 
	 * "doctor bob at sims" + " SIMS ACCOUNT at Caisi Integrator" + "Caisi Integrator account at HNR"
	 * so you can trace back the entire path of who triggered this.
	 */
	public void setAccessorTrail(String accessorTrail)
	{
		accessorTrail = StringUtils.trimToNull(accessorTrail);
		this.accessorTrail = accessorTrail;
	}

	public Date getDate()
	{
		return date;
	}

	public void setDate(Date date)
	{
		this.date = date;
	}

	public String getAccessorTrail()
	{
		return accessorTrail;
	}

	public EventType getEventType()
	{
		return eventType;
	}

	public void setEventType(EventType eventType)
	{
		if (eventType == null) throw(new IllegalArgumentException("event can't be null"));
		this.eventType = eventType;
	}

	public Integer getLocalUserId()
	{
		return localUserId;
	}

	/**
	 * The localUserId is the local userId initiating this request.
	 * i.e. the HNR user.id, or null for internal system/code events.
	 */
	public void setLocalUserId(Integer localUserId)
	{
		this.localUserId = localUserId;
	}

	public String getLocalSourceCode()
	{
		return localSourceCode;
	}

	/**
	 * The localSourceCode should be a human readable string denoting origins of the request from a source code
	 * point of view. i.e. if the work is done on behalf of a thread or background task
	 * this should probably be the thread class name. If it's a jsp request it could
	 * simply be the URI or servlet name, if it's a web service it could be the 
	 * service end point with possibly the method.
	 */
	public void setLocalSourceCode(String localSourceCode)
	{
		this.localSourceCode = localSourceCode;
	}

	public String getDataType()
	{
		return dataType;
	}

	/**
	 * Data type is what type of data was accessed.
	 * For all intent and purposes the DAO model class name should suffice in most cases.
	 */
	public void setDataType(String dataType)
	{
		this.dataType = dataType;
	}

	public String getDataId()
	{
		return dataId;
	}

	/**
	 * This is the ID for the piece of data accessed. i.e. if it's 
	 * a dao / model object, it would the the model objects getId().
	 */
	public void setDataId(String dataId)
	{
		this.dataId = dataId;
	}

	@PreRemove
	protected void jpaPreventDelete()
	{
		throw(new UnsupportedOperationException("Remove is not allowed for this type of item."));
	}

	@PreUpdate
	protected void jpaPreventUpdate()
	{
		throw(new UnsupportedOperationException("Update is not allowed for this type of item."));
	}
}
