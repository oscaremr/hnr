package org.oscarehr.hnr.dao;

public class MatchingClientScore implements Comparable<MatchingClientScore>
{
	private Client client = null;
	private int score = 0;

	public Client getClient()
	{
		return client;
	}

	public void setClient(Client client)
	{
		this.client = client;
	}

	public int getScore()
	{
		return score;
	}

	public void setScore(int score)
	{
		this.score = score;
	}

	@Override
    public int compareTo(MatchingClientScore o)
    {
	    return score-o.score;
    }
}