package org.oscarehr.hnr.dao;

import org.oscarehr.hnr.dao.EventLog.EventType;
import org.oscarehr.hnr.util.LoggedInInfo;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * We don't want logging in the same transaction as regular work.
 */
@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class EventLogDao extends AbstractDao<EventLog>
{
	/**
	 * This variable is used to override the default eventType used for read/write access auditing.
	 * The main inspiration for this was so searches can set the eventType from READ to SEARCH.
	 * Usage of this should follow the try/finally idiom of JDBC connections, i.e.
	 * set the override right before you do some audited operation, and in the finally block
	 * you must clear the override (or some one else will get your override).
	 */
	protected static final ThreadLocal<EventLog.EventType> eventTypeOverride = new ThreadLocal<EventLog.EventType>();
	
	public EventLogDao()
	{
		modelClass = EventLog.class;
	}

	/**
	 * This is part of a series of convenience methods for logging events.
	 */
	public EventLog createEvent(EventType eventTypeDefault, AbstractModel<?> model)
	{
		EventType eventType=eventTypeOverride.get();
		if (eventType==null) eventType=eventTypeDefault;
		
		return(createEvent(eventType, model.getClass().getName(), model.getId()));
	}

	/**
	* This is part of a series of convenience methods for logging events.
	*/
	public EventLog createEvent(EventType eventType, String dataType, Object dataId)
	{
		EventLog eventLog = new EventLog();

		LoggedInInfo loggedInInfo = LoggedInInfo.loggedInInfo.get();

		// some basic sanity checks
		if (loggedInInfo == null) throw(new SecurityException("Audited item requires logged in information but loggedInInfo was never set."));

		if (loggedInInfo.loggedInUser == null && loggedInInfo.initiatingCode == null)
			throw(new SecurityException("Audited item requires logged in information but both user and initiatingCode are null."));
		
		// store the data
		if (loggedInInfo.loggedInUser != null) eventLog.setLocalUserId(loggedInInfo.loggedInUser.getId());

		eventLog.setAccessorTrail(loggedInInfo.loggedInUserAuditTrail);
		eventLog.setLocalSourceCode(loggedInInfo.initiatingCode);
		
		if (dataId != null) eventLog.setDataId(dataId.toString());
		eventLog.setDataType(dataType);
		eventLog.setEventType(eventType);

		persist(eventLog);

		return(eventLog);
	}

	@Override
	public void refresh(EventLog o)
	{
		throw(new UnsupportedOperationException("can't refresh an unupdateable object"));
	}
}
