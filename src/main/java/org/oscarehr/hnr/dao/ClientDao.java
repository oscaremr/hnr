package org.oscarehr.hnr.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;
import org.oscarehr.hnr.util.AccumulatorMap;
import org.oscarehr.hnr.util.AdditionalSchemaGenerationSql;
import org.oscarehr.hnr.util.JpaUtils;
import org.springframework.stereotype.Repository;

@Repository
public class ClientDao extends AbstractDao<Client>
{
	public ClientDao()
	{
		modelClass = Client.class;
	}

	public Client findByLinkingIdUnlockedUnhidden(Integer linkingId)
	{
		Client client = find(linkingId);
		if (client == null) return(null);

		if (client.isLockbox() || client.isHidden()) client = null;
		return(client);
	}

	/**
	 * Find people who might be the same person
	 * @return a list of Map.Entry<Client, Integer> where Integer is their score, it is sorted by highest score first.
	 */
	public ArrayList<MatchingClientScore> findMatchingClients(int maxEntriesToReturn, int minimumScore, String firstName, String lastName, Date birthDate, String hin, String gender, String sin, String province, String city)
	{
		EventLogDao.eventTypeOverride.set(EventLog.EventType.SEARCH_LIST);
		
		try
		{
			// trim parameters to null, avoids giving points to "" matching ""
			firstName = StringUtils.trimToNull(firstName);
			lastName = StringUtils.trimToNull(lastName);
			hin = StringUtils.trimToNull(hin);
			gender = StringUtils.trimToNull(gender);
			sin = StringUtils.trimToNull(sin);
			province = StringUtils.trimToNull(province);
			city = StringUtils.trimToNull(city);

			//------------
			// score board
			//------------
			// we want the total max score to be 100 (or really close but less) for easy end user comprehension

			String baseSqlStatement = "select * from " + modelClass.getSimpleName() + " where hidden=?1 and lockbox=?2";

			// this map contains demographicIds.
			AccumulatorMap<Client> scoreMap = new AccumulatorMap<Client>();

			if (hin != null) findAndAddScore(scoreMap, 22, baseSqlStatement + " and hin=?3", hin);
			if (sin != null) findAndAddScore(scoreMap, 20, baseSqlStatement + " and sin=?3", sin);

			// keep in mind the name matching is cumulative, i.e. an exact match should match 3 times here for 3 times the score.
			if (firstName != null) findAndAddScore(scoreMap, 6, baseSqlStatement + " and firstName sounds like ?3", firstName);
			if (lastName != null) findAndAddScore(scoreMap, 7, baseSqlStatement + " and lastName sounds like ?3", lastName);
			if (firstName != null) findAndAddScore(scoreMap, 5, baseSqlStatement + " and firstName like ?3", "%" + firstName + "%");
			if (lastName != null) findAndAddScore(scoreMap, 2, baseSqlStatement + " and lastName like ?3", "%" + lastName + "%");
			if (firstName != null) findAndAddScore(scoreMap, 6, baseSqlStatement + " and firstName=?3", firstName);
			if (lastName != null) findAndAddScore(scoreMap, 7, baseSqlStatement + " and lastName=?3", lastName);

			if (birthDate != null) findAndAddScore(scoreMap, 15, baseSqlStatement + " and birthDate=?3", birthDate);
			if (gender != null) findAndAddScore(scoreMap, 5, baseSqlStatement + " and gender=?3", gender);
			if (province != null) findAndAddScore(scoreMap, 2, baseSqlStatement + " and province=?3", province);
			if (city != null) findAndAddScore(scoreMap, 3, baseSqlStatement + " and city=?3", city);

			List<Entry<Client, Integer>> sorted = scoreMap.getEntriesSortedByValue(false).subList(0, Math.min(scoreMap.size(), maxEntriesToReturn));

			Iterator<Entry<Client, Integer>> it = sorted.iterator();
			ArrayList<MatchingClientScore> results = new ArrayList<MatchingClientScore>();
			while (it.hasNext())
			{
				Entry<Client, Integer> entry = it.next();
				if (entry.getValue() >= minimumScore)
				{
					MatchingClientScore x = new MatchingClientScore();
					x.setClient(entry.getKey());
					x.setScore(entry.getValue());
					results.add(x);
				}
			}

			return(results);
		}
		finally
		{
			EventLogDao.eventTypeOverride.remove();
		}
	}

	private void findAndAddScore(AccumulatorMap<Client> scoreMap, int score, String sqlCommand, Object parameter)
	{
		Query query = entityManager.createNativeQuery(sqlCommand, modelClass);
		int counter = 1;
		query.setParameter(counter++, false);
		query.setParameter(counter++, false);

		if (parameter instanceof Date) query.setParameter(counter++, (Date)parameter, TemporalType.DATE);
		else query.setParameter(counter++, parameter);

		@SuppressWarnings("unchecked")
		List<Client> results = query.getResultList();
		for (Client client : results)
		{
			scoreMap.increment(client, score);
		}
	}

	/**
	 * This method is meant to be called when ever a change takes place to the client record.
	 * i.e. the PreUpdate method of the jpa object.
	 */
	protected static void archivePreviousRecords(Integer clientId)
	{
		// since this is run during the commit phase of
		// a jpa update, we need a new transaction / entity manager or else 
		// this one will collide with the existing one
		// since this is a native query it'll attempt 
		// to commit right away.
		
		if (clientId==null) return;
		
		EntityManager entityManager=JpaUtils.createEntityManager();
		try
		{
			EntityTransaction tx=entityManager.getTransaction();
			tx.begin();
			
			Query query=entityManager.createNativeQuery("insert into ClientArchive select *,now() from Client where id=?1");
			query.setParameter(1,clientId);
			query.executeUpdate();
			
			tx.commit();
		}
		finally
		{
			JpaUtils.close(entityManager);
		}
	}

	public int countArchivedRecords()
	{
		Query query=entityManager.createNativeQuery("select count(*) from ClientArchive", Integer.class);
		return((Integer)query.getSingleResult());
	}
	
	@AdditionalSchemaGenerationSql
	public String[] getAdditionalCustomSql()
	{
		String[] sql =
		{
			"create table ClientArchive like Client",
			"alter table ClientArchive modify id int not null",
			"alter table ClientArchive drop primary key",
			"alter table ClientArchive drop index UNQ_",
			"alter table ClientArchive add column (archiveDate datetime not null)",
		};

		return(sql);
	}
}
