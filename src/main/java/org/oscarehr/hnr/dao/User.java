package org.oscarehr.hnr.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PreRemove;

import org.oscarehr.hnr.util.MiscUtils;

/**
 * This class is used to control who has access to read/write from the HNR.
 * Users should not be deleted once created because data audit logging is foreign keyed
 * off if this. To disable a user just change their password.
 */
@Entity
public class User extends AbstractModel<Integer>
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id=null;

	@Column(nullable=false, unique=true, length=32)
	private String name=null;
	
	@Column(nullable=false, length=32)
	private String password=null;
	
	@Column(nullable=false, length=255)
	private String contactInfo=null;

	@Override
	public Integer getId()
	{
		return(id);
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name=MiscUtils.validateAndNormaliseUserName(name);
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		if (password==null) throw(new IllegalArgumentException("password can't be null"));
		this.password=password;
	}

	public String getContactInfo()
	{
		return contactInfo;
	}

	/**
	 * Contact info for a user is a human readable contact information.
	 * It is what ever is necessary to contact this user, i.e. could be
	 * phone number, email address, physical address or stuff like that.
	 */
	public void setContactInfo(String contactInfo)
	{
		if (contactInfo==null) throw(new IllegalArgumentException("contactInfo can't be null"));
		this.contactInfo=contactInfo;
	}
	
	@PreRemove
	protected void jpaPreventDelete()
	{
		throw(new UnsupportedOperationException("Remove is not allowed for this type of item."));
	}
}
