package org.oscarehr.hnr.ws.client_tests;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.oscarehr.hnr.ws.ClientWs;
import org.oscarehr.hnr.ws.ClientWsService;
import org.oscarehr.hnr.ws.MatchingClientParameters;
import org.oscarehr.hnr.ws.MatchingClientScore;

public class ClientTestClient
{
	public static void main(String... argv) throws Exception
	{
		ClientWsService service = new ClientWsService();
		ClientWs clientPort = service.getClientWsPort();

		CxfClientUtils.configureSsl(clientPort);
		CxfClientUtils.addWSS4JAuthentication("test_user", "test_password", clientPort);		

		MatchingClientParameters matchingParameters=new MatchingClientParameters();
		matchingParameters.setMaxEntriesToReturn(10);
		matchingParameters.setMinScore(1);
		matchingParameters.setFirstName("first name");
		
		// print client info
		for (MatchingClientScore match : clientPort.getMatchingClients(matchingParameters))
		{
			System.err.println(""+match.getScore()+", "+ReflectionToStringBuilder.toString(match.getClient()));
		}
	}

}