package org.oscarehr.hnr.ws.client_tests;

import org.oscarehr.hnr.ws.HelloWorldWs;
import org.oscarehr.hnr.ws.HelloWorldWsService;

public class HelloWorldTestClient
{
	public static void main(String... argv) throws Exception
	{
		HelloWorldWsService service = new HelloWorldWsService();
		HelloWorldWs helloWorld = service.getHelloWorldWsPort();
		
		CxfClientUtils.configureSsl(helloWorld);
		
		System.err.println(helloWorld.helloWorld());
		System.err.println(helloWorld.helloWorld2("foo"));
	}
}
